import { Type } from 'class-transformer';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class FilterBookDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  category: string;

  @IsOptional()
  @IsInt()
  @Type(() => Number)
  min_year: number;

  @IsOptional()
  @IsInt()
  @Type(() => Number)
  max_year: number;
}
